﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LandingPage_Default" %>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Escape to Tulum</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
    <link rel="preconnect" href="https://cdnjs.cloudflare.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap"
      rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/cgs2dgh.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/tulum/css/site.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/tulum/img/favicon/favicon.png" />
    <link rel="apple-touch-icon" href="/tulum/img/favicon/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/tulum/imgfavicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/tulum/imgfavicon/apple-touch-icon-114x114.png" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162929067-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', 'UA-162929067-2');
    </script>
  </head>

  <body>
    <header id="frmtHead">
      <div class="container">
        <div class="row">
          <div class="col-4">
            <div id="lang-wrapper">
              <ul tabindex="-1">
                <li>
                  <a href="#" class="active">
                    English
                  </a>
                </li>
                <li>
                  <a href="/tulumes">
                    Español
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-4 text-center">
            <a href="/">
              <img src="/tulum/img/blank.png" data-src="/tulum/img/farmasi-rednwhite.png" class="lazyload img-fluid"
                alt="Escape to Tulum" width="170" height="40">
            </a>
          </div>
          <div class="col-4 text-right">
            <button id="menu-btn" onclick="menuOpen()">
              <svg style="width: 24px; height: 24px" viewBox="0 0 24 24">
                <path fill="#fff" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z"></path>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </header>
    <div id="menu">
      <button class="close-menu" onclick="menuClose()">
        <svg style="width: 24px; height: 24px" viewBox="0 0 24 24">
          <path fill="#fff"
            d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z">
          </path>
        </svg>
      </button>
      <ul id="menuList">
        <li><a href="#section1" class="scroll">Escape to Tulum!</a></li>
        <li><a href="#video" class="scroll">Video</a></li>
        <li><a href="#section3" class="scroll">Farmasi Tulum</a></li>
        <li><a href="#section4" class="scroll">What's Included</a></li>
        <li><a href="#section5" class="scroll">Qualify for Tulum</a></li>
        <li><a href="#section6" class="scroll">Farmasi Adventure</a></li>
      </ul>
    </div>
    <section id="section1" class="jarallax" data-jarallax data-speed="0.5">
      <picture class="jarallax-img">
        <source media="(max-width: 1023px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section1m.jpg" --small>
        <source media="(min-width: 1024px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section1.jpg" --medium>
        <img srcset="/tulum/img/blank.png" data-src="/tulum/img/section1.jpg" alt="Escape to Tulum" class="lazyload">
      </picture>
    </section>
    <section id="video">
      <div class="vid-container">
        <iframe width="560" height="315" data-src="https://www.youtube.com/embed/u8RWfoc1f9U?rel=0" frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen="" class="lazyload jarallax-img"></iframe>
      </div>
    </section>
    <section id="section3">
      <picture>
        <source media="(max-width: 1023px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section3m.svg" --small>
        <source media="(min-width: 1024px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section3.jpg" --medium>
        <img srcset="/tulum/img/blank.png" data-src="/tulum/img/section3.jpg" alt="Escape to Tulum" class="lazyload">
      </picture>
    </section>
    <section id="section4" class="jarallax" data-jarallax data-speed="0.5">
      <picture class="jarallax-img">
        <source media="(max-width: 1023px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section4m.svg" --small>
        <source media="(min-width: 1024px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section4.jpg" --medium>
        <img srcset="/tulum/img/blank.png" data-src="/tulum/img/section4.jpg" alt="Escape to Tulum" class="lazyload">
      </picture>
    </section>
    <section id="section5">
      <picture>
        <source media="(max-width: 1023px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section5m.svg" --small>
        <source media="(min-width: 1024px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section5.jpg" --medium>
        <img srcset="/tulum/img/blank.png" data-src="/tulum/img/section5.jpg" alt="Escape to Tulum" class="lazyload">
      </picture>
    </section>
    <section id="section6">
      <picture>
        <source media="(max-width: 1023px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section6m.jpg" --small>
        <source media="(min-width: 1024px)" srcset="/tulum/img/blank.png" data-srcset="/tulum/img/section6.jpg" --medium>
        <img srcset="/tulum/img/blank.png" data-src="/tulum/img/section6.jpg" alt="Escape to Tulum" class="lazyload">
      </picture>
    </section>
    <footer id="frmFooter">
      <ul>
        <li>
          <a href="https://facebook.com/Farmasi-USA-2234387646887335/" target="_blank">
            <img src="/tulum/img/blank.png" data-src="/tulum/img/facebook.png" alt="Escape to Tulum" class="lazyload">
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/farmasiusa/" target="_blank">
            <img src="/tulum/img/blank.png" data-src="/tulum/img/instagram.png" alt="Escape to Tulum" class="lazyload instagram">
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/channel/UCa0BWzAqhRML6yG5CRRBhNQ?view_as=subscriber" target="_blank">
            <img src="/tulum/img/blank.png" data-src="/tulum/img/youtube.png" alt="Escape to Tulum" class="lazyload">
          </a>
        </li>
      </ul>
      <p>© 2021 Farmasi</p>
    </footer>
    <script src="/tulum/js/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.3.2/lazysizes.min.js"
      integrity="sha512-q583ppKrCRc7N5O0n2nzUiJ+suUv7Et1JGels4bXOaMFQcamPk9HjdUknZuuFjBNs7tsMuadge5k9RzdmO+1GQ=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"></script>
    <script src="/tulum/js/jarallax.min.js"></script>
    <script src="/tulum/js/core.min.js"></script>
  </body>

  </html>